using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destruir : MonoBehaviour
{
    public AudioSource source;
    public AudioClip audioFX;
    public int vidasEnemigos;
    public Animator Animacion;
    public Movement velocidad;

    private void Start()
    {

    }

    public void OnMouseDown()
    {
        vidasEnemigos--;


        if (vidasEnemigos <= 0)
        {
            velocidad.velocidadMovimiento = 0f;

            Animacion.SetBool("moricion", true);
        }
        

    }

    private void MyOnDestroy()
    {
        Destroy(gameObject);   
    }

    public void MyOnSOund() 
    {
        source.PlayOneShot(audioFX);
    }
}
