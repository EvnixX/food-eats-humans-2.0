using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Detectar : MonoBehaviour
{
    public GameObject[] corazones;
    public int vida;
    public GameObject[] enemigos;

    private void Update()
    {
        if (vida < 1)
        {
            Destroy(corazones[0].gameObject);
            SceneManager.LoadScene("Perdiste");
        }
        else if (vida < 2)
        {
            Destroy(corazones[1].gameObject);
        }
        else if (vida < 3)
        {
            Destroy(corazones[2].gameObject);
        }
    }

    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemigo")
        {
            vida--;
            Destroy(collision.gameObject);
        }

        if (collision.tag == "Humano")
        {
          
            Destroy(collision.gameObject);
        }
    }
}

