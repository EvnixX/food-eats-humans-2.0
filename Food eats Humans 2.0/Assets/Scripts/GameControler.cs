using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControler : MonoBehaviour
{
    public static int Score = 0;
    public float highscore;
    public string ScoreString = "Score";

    public Text TextScore;
    public Text Texthighscore;

    public static GameControler GameController;

    public void Awake()
    {
        GameController = this;
    }

    public void AddScore()
    {
        Score++;
    }

    private void Start()
    {
        Score = 0;
        highscore = PlayerPrefs.GetFloat("Highscore");
    }

    private void Update()
    {
        if(TextScore != null)
        {
            TextScore.text = Score.ToString();
            Texthighscore.text = highscore.ToString();
        }
        if(Score > highscore)
        {
            PlayerPrefs.SetFloat("Highscore", Score);
        }
        
    }
}
