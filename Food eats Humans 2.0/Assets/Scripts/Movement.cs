using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] public float velocidadMovimiento;
    [SerializeField] private Transform[] puntoMovimiento;
    [SerializeField] private float distanciaMin;
    
    private int numeroAleatorio;
    private SpriteRenderer spriteRenderer;

    private void Start()
    {
        numeroAleatorio = Random.Range(0, puntoMovimiento.Length);
        spriteRenderer = GetComponent<SpriteRenderer>();
        Girar();

    }

    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, puntoMovimiento[numeroAleatorio].position, velocidadMovimiento * Time.deltaTime);
        if (Vector2.Distance(transform.position, puntoMovimiento[numeroAleatorio].position) < distanciaMin)
        {
            numeroAleatorio = Random.Range(0, puntoMovimiento.Length);
            Girar();
        }
    }
    private void Girar()
    {
        if (transform.position.x < puntoMovimiento[numeroAleatorio].position.x)
        {
            spriteRenderer.flipX = true;
        }
        else
        {
            spriteRenderer.flipX = false;
        }
    }
}
