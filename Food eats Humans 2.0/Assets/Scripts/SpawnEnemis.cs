using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemis : MonoBehaviour
{
    public GameObject[] enemies;

    public float timeSpawn = 1;
    public float repeatSpawnRate = 3;

    public Transform xRangeLeft;
    public Transform xRangeRight;
    public Transform yRangeUp;
    public Transform yRangeDown;

    public float difficultyTime = 0;

    void Start()
    {
        StartCoroutine("EnemyDifficulty");
    }

    public void Update()
    {
        difficultyTime += Time.deltaTime;

        if (difficultyTime > 10 && difficultyTime < 20)
        {
            repeatSpawnRate = 3;
        }

        if (difficultyTime > 20 && difficultyTime < 30)
        {
            repeatSpawnRate = 2;
        }

        if (difficultyTime > 30 && difficultyTime < 40)
        {
            repeatSpawnRate = 1;
        }

        if (difficultyTime > 40 && difficultyTime < 50)
        {
            repeatSpawnRate = 0.75f;
        }

    }

    IEnumerator EnemyDifficulty()
    {
        yield return new WaitForSeconds(repeatSpawnRate);
        SpawnEnemies();
        StartCoroutine("EnemyDifficulty");

    }
    public void SpawnEnemies()
    {
        Vector3 spawnPosition = new Vector3(0, 0, 0);
        spawnPosition = new Vector3(Random.Range(xRangeLeft.position.x, xRangeRight.position.x), Random.Range(yRangeDown.position.y, yRangeUp.position.y), 0);
        GameObject enemie = Instantiate(enemies[Random.Range(0, enemies.Length)], spawnPosition, gameObject.transform.rotation);
    }

}
